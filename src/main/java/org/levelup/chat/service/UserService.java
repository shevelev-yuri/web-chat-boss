package org.levelup.chat.service;

import org.levelup.chat.domain.dto.User;

import java.util.Collection;

public interface UserService {

    boolean auth(String login, String password);

    Collection<User> getAll();

}
