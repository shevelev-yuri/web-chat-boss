package org.levelup.chat.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:database.properties")
public class JpaConfiguration {

    @Value("${chat.db.host}")
    private String host;
    @Value("${chat.db.name}")
    private String dbName;
    @Value("${chat.db.port}")
    private String port;
    @Value("${chat.db.username}")
    private String username;
    @Value("${chat.db.password}")
    private String password;

    @Bean
    public DataSource dataSource() {
        System.out.println("PO");
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl(String.format("jdbc:postgresql://%s:%s/%s", host, port, dbName));
        return dataSource;
    }

}
